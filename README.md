# OTA Update Server #

Android OTA update server.


## Docker Image ##

To create the docker image run:

```
$ gradle dockerBuildImage
```

This will create/update the docker image named `com.videray/otaserver`

## Webserver Launch

Launch the stand-alone server with command line:
```
$ java -jar OTAServer.jar
```

In the same directory, you should have the `config/application.properties` file. At a minimum, the application.properties file should contain:

```properties
signingKey=/data/projects/firmware/build/target/product/security/testkey
pathPrefix=/data/projects/firmware/out/host/linux-x86
```

The server watches the `otaupdates/drop` dir for new targetfiles.zip (created via running `make dist` in ASOP) and processes them into update files. It does this by reading the SYSTEM/build.prop looking for:


```properties
ro.product.device=<your_device_name>
ro.build.channel=<alpha|beta|release>
ro.build.version.id=<0.1.0>
```

Incremental or full update files are created on-demand when the REST Client makes requests. OTGServer will convert TargetFiles.zip into update files using the embedded releasetools. The release tools was copied from AOSP v9 (build/tools/releasetools).

#### Index File

In additon to the drop folder method, OTAServer also accepts adding full update files directly via Index file. The index file is optional.

`index.json`

```json
{
  "updateFiles": [
    {
      "path": "path/to/full_update_file.zip",
      "device": "",
      "channel": "",
      "version": "1.0.0"
    }
  ]
}

```

## REST API ##

### Get update ###

Called by the client device to check for existence of an update file.

**URL** : `/api/v1/update`

**Method** : `POST`

 ```json
{
  "device": "<some unique id for hardware>",
  "channel": "alpha|beta|release|etc",
  "current_version": "1.3.0",
  "current_channel": "alpha|beta|release|etc"
}
```

#### Success Response ####

**CODE** : `200`

```json
{
  "available": true,
  "version": "<new version available>",
  "release_notes": "<brief description of changes>",
  "downloadId": "<id used for download request>"
}
```

**OR**

**CODE** : `201`

```json
{
  "available": false
}
```


### Download ###

Called by the client to download the update file.

**URL** : `/api/v1/download/:downloadId`

**Method** : `GET`

The value of `downloadId` is obtained from the `/api/v1/update` response.

