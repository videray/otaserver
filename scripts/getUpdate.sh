#!/bin/bash

URL=https://update.videray.com
#URL=http://localhost:8080

curl ${URL}/api/v1/update -X POST \
    -H "Content-Type: application/json" \
    --data '{"device": "flintlock", "channel": "beta", "current_version": "0.0.1", "current_channel": "release"}'
