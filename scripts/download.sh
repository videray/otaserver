#!/bin/bash

downloadId=$1

#URL=http://localhost:8080
URL=https://update.videray.com

curl ${URL}/api/v1/download/${downloadId} -X GET \
    --output downloadfile.zip
