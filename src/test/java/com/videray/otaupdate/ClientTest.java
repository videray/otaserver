package com.videray.otaupdate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.videray.otaupdate.api.UpdateRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ClientTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private UpdateManager updateManager;

    @Test
    public void getUpdate_newversionavailable() throws Exception {

        final String device = "flintlock";
        final String channel = "release";
        final String downloadId = "328a0b9";
        final VersionKey oldVersion = new VersionKey(device, channel, new Version(0, 2, 0));
        final VersionKey newVersion = new VersionKey(device, channel, new Version(1, 0, 0));


        Mockito.doReturn(new Version(1, 0, 0)).when(updateManager).getLatestFor(device, channel);
        Mockito.doReturn(false).when(updateManager).hasTargetFiles(oldVersion);
        Mockito.doReturn(true).when(updateManager).hasTargetFiles(newVersion);
        Mockito.doReturn(downloadId).when(updateManager).getFullUpdate(newVersion);



        UpdateRequest updateRequest = new UpdateRequest();
        updateRequest.device = "flintlock";
        updateRequest.current_channel = "release";
        updateRequest.channel = "release";
        updateRequest.current_version = "0.2.0";

        MockHttpServletRequestBuilder request = post("/api/v1/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateRequest));

        mvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.available", is(true)))
                .andExpect(jsonPath("$.version", is("1.0.0")))
                .andExpect(jsonPath("$.channel", is("release")))
                .andExpect(jsonPath("$.downloadId", is(downloadId)))
        ;
    }

    @Test
    public void getUpdate_notavailable() throws Exception {
        final String device = "flintlock";
        final String channel = "release";
        final String downloadId = "328a0b9";
        final VersionKey key = new VersionKey(device, channel, new Version(0, 2, 0));


        Mockito.doReturn(key.version).when(updateManager).getLatestFor(device, channel);
        Mockito.doReturn(true).when(updateManager).hasTargetFiles(key);

        UpdateRequest updateRequest = new UpdateRequest();
        updateRequest.device = device;
        updateRequest.current_channel = channel;
        updateRequest.channel = channel;
        updateRequest.current_version = "0.2.0";

        MockHttpServletRequestBuilder request = post("/api/v1/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateRequest));

        mvc.perform(request)
                .andExpect(status().is(201))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.available", is(false)))
                .andExpect(jsonPath("$.downloadId", isEmptyOrNullString() ))
        ;
    }

    @Test
    public void getUpdate_nonexistingdevice() throws Exception {
        final String device = "flintlock";
        final String channel = "release";
        final String downloadId = "328a0b9";
        final VersionKey key = new VersionKey(device, channel, new Version(0, 2, 0));


        Mockito.doReturn(null).when(updateManager).getLatestFor(device, channel);
        Mockito.doReturn(false).when(updateManager).hasTargetFiles(key);

        UpdateRequest updateRequest = new UpdateRequest();
        updateRequest.device = device;
        updateRequest.current_channel = channel;
        updateRequest.channel = channel;
        updateRequest.current_version = "0.2.0";

        MockHttpServletRequestBuilder request = post("/api/v1/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateRequest));

        mvc.perform(request)
                .andExpect(status().is(201))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.available", is(false)))
                .andExpect(jsonPath("$.downloadId", isEmptyOrNullString() ))
        ;
    }


}
