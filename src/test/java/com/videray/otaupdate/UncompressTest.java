package com.videray.otaupdate;

import org.junit.Test;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class UncompressTest {

    @Test
    public void unzipTargetFilesIntoUncompressedZip() throws Exception {

        byte[] buf = new byte[2048];
        ZipEntry ze;
        int len;

        FileOutputStream fout = new FileOutputStream(new File("testoutput.zip"));
        ZipOutputStream zipOut = new ZipOutputStream(fout);
        zipOut.setMethod(ZipOutputStream.STORED);


        FileInputStream fin = new FileInputStream(new File("signed-target_files_31.zip"));
        ZipInputStream zipIn = new ZipInputStream(new BufferedInputStream(fin));



        File tmpFile = File.createTempFile("unziptmpfile", ".dat");
        try {

            while ((ze = zipIn.getNextEntry()) != null) {

                try (FileOutputStream tmpOut = new FileOutputStream(tmpFile)) {
                    while ((len = zipIn.read(buf)) > 0) {
                        tmpOut.write(buf, 0, len);
                    }
                }

                ze.setMethod(ZipOutputStream.STORED);
                ze.setCompressedSize(ze.getSize());
                zipOut.putNextEntry(ze);

                try (FileInputStream tmpIn = new FileInputStream(tmpFile)) {
                    while ((len = tmpIn.read(buf)) > 0) {
                        zipOut.write(buf, 0, len);
                    }
                }


                zipOut.closeEntry();


            }
        } finally {
            tmpFile.delete();
            zipIn.close();
            zipOut.close();
        }




    }
}
