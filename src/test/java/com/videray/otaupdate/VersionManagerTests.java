package com.videray.otaupdate;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class VersionManagerTests {

    @Test
    public void testGetLatestVersion_exists() throws IOException {
        VersionManager manager = new VersionManager();
        manager.add(new VersionKey("zebra", "release", Version.fromString("1.1.0")));
        manager.add(new VersionKey("marlin", "release", Version.fromString("1.0.0")));
        manager.add(new VersionKey("marlin", "release", Version.fromString("1.0.1")));
        manager.add(new VersionKey("marlin", "release", Version.fromString("1.1.0")));
        manager.add(new VersionKey("marlin", "beta", Version.fromString("1.0.0")));
        manager.add(new VersionKey("goldburger", "release", Version.fromString("1.1.0")));

        Version version = manager.getLatestFor("marlin", "release");
        assertNotNull(version);
        assertEquals(version, Version.fromString("1.1.0"));
    }

    @Test
    public void testGetLatestVersion_nonexistingdevice() throws IOException {
        VersionManager manager = new VersionManager();
        manager.add(new VersionKey("zebra", "release", Version.fromString("1.1.0")));
        manager.add(new VersionKey("marlin", "release", Version.fromString("1.0.0")));
        manager.add(new VersionKey("marlin", "release", Version.fromString("1.1.0")));
        manager.add(new VersionKey("marlin", "beta", Version.fromString("1.0.0")));
        manager.add(new VersionKey("goldburger", "release", Version.fromString("1.1.0")));

        Version version = manager.getLatestFor("funky", "release");
        assertNull(version);
    }

    @Test
    public void testGetLatestVersion_nonexistingchannel() throws IOException {
        VersionManager manager = new VersionManager();
        manager.add(new VersionKey("zebra", "release", Version.fromString("1.1.0")));
        manager.add(new VersionKey("marlin", "release", Version.fromString("1.0.0")));
        manager.add(new VersionKey("marlin", "release", Version.fromString("1.1.0")));
        manager.add(new VersionKey("marlin", "beta", Version.fromString("1.0.0")));
        manager.add(new VersionKey("goldburger", "release", Version.fromString("1.1.0")));

        Version version = manager.getLatestFor("marlin", "alpha");
        assertNull(version);
    }


}
