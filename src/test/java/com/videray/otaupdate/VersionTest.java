package com.videray.otaupdate;

import org.junit.Test;
import static org.junit.Assert.*;

public class VersionTest {

    @Test
    public void testParse() throws Exception {
        Version v = Version.fromString("0.2.4");
        assertNotNull(v);
    }

    @Test
    public void testEquals() {
        Version a = new Version(0, 1, 2);
        Version b = new Version(0, 1, 2);
        assertEquals(a, b);
    }

    @Test
    public void testCompare() {
        Version a = new Version(5, 2, 0);
        Version b = new Version(5, 3, 0);
        assertTrue(a.compareTo(b) < 0);
    }
}
