package com.videray.otaupdate;

import com.google.common.io.Files;
import com.google.common.io.Resources;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

public class UpdateManagerTests {

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();


    @Test
    public void loadBuildPropTest() throws IOException {

        File updateFile = folder.newFile("target_files.zip");


        Resources.asByteSource(Resources.getResource("example-target_files-eng.zip"))
                .copyTo(Files.asByteSink(updateFile));

        VersionKey key = UpdateManager.createVersionKey(updateFile);
        assertNotNull(key);
        assertEquals("flintlock", key.device);
        assertEquals("beta", key.channel);
        assertEquals(new Version(0, 1, 0), key.version);

    }


}
