package com.videray.otaupdate;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class Static {

    public static final ScheduledExecutorService main = Executors.newSingleThreadScheduledExecutor();
    public static final ExecutorService workers = Executors.newCachedThreadPool();
}
