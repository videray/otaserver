package com.videray.otaupdate;

import com.google.common.io.Files;
import com.videray.otaupdate.tasks.RemoveDeflateTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.nio.file.*;
import java.util.concurrent.TimeUnit;

@Service
public class FileDropWatcher {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileDropWatcher.class);
    private static final Object sScanLock = new Object();

    private boolean mRunning = false;


    @Autowired
    @Qualifier(OTAApp.DROP_DIR)
    private File rootDir;

    @Autowired
    private UpdateManager updateManager;

    @Autowired
    Config config;

    @PostConstruct
    public void init() {
        Static.main.execute(() -> {
            Static.workers.submit(() -> {
                scan();
                Static.main.execute(() -> start());
            });
        });
    }

    public void scan() {
        synchronized (sScanLock) {
            LOGGER.info("scanning drop dir: {}", rootDir);
            File[] files = rootDir.listFiles();
            if (files != null) {
                for (File f : files) {
                    updateManager.getExecutor().execute(() -> processFile(f));
                }
            }
        }
    }

    public void start() {
        if(!mRunning) {
            mRunning = true;
            Static.workers.execute(mWatcherTask);
        } else {
            LOGGER.warn("already running");
        }

    }

    private void processFile(File f) {
        String name = f.getName();
        if(name.endsWith(".zip")) {
            try {
                VersionKey versionKey = UpdateManager.createVersionKey(f);
                File dest_file = updateManager.getTargetFilesZip(versionKey);
                if (dest_file.exists()) {
                    LOGGER.warn("target file already exists {}", versionKey);
                    f.delete();
                } else {
                    if(config.isRemoveDeflate()) {
                        RemoveDeflateTask removeDeflateTask = new RemoveDeflateTask();
                        removeDeflateTask.inputZipFile = f;
                        removeDeflateTask.outputZipFile = dest_file;
                        removeDeflateTask.run();
                        f.delete();
                    } else {
                        Files.move(f, dest_file);
                    }
                    updateManager.addVersion(versionKey);
                }
            } catch (Exception e) {
                LOGGER.error("could not process targetfiles: {}", f, e);
                f.delete();
            }
        }
    }

    public void stop() {
        mRunning = false;
    }

    private final Runnable mWatcherTask = new Runnable() {
        @Override
        public void run() {
            try {
                LOGGER.info("Start watching dir: {}", rootDir.getAbsolutePath());

                WatchService watchService = FileSystems.getDefault().newWatchService();
                Path path = rootDir.toPath();
                path.register(watchService,
                        StandardWatchEventKinds.ENTRY_CREATE);


                while(mRunning) {
                    WatchKey key = watchService.poll(500, TimeUnit.MILLISECONDS);
                    if(key != null) {
                        try {

                            for(WatchEvent<?> event : key.pollEvents()) {
                                WatchEvent.Kind<?> kind = event.kind();

                                if(kind == StandardWatchEventKinds.ENTRY_CREATE) {
                                    Path filepath = (Path) event.context();
                                    final File newFile = new File(rootDir, filepath.toString());
                                    LOGGER.info("new file found: {}", newFile);
                                    Static.workers.execute(() -> {
                                        try {
                                            while (!isCompletelyWritten(newFile)) {
                                                LOGGER.info("waiting for {} to finish...", newFile);
                                            }
                                            updateManager.getExecutor().execute(() -> processFile(newFile));
                                        } catch (Exception e) {
                                            LOGGER.error("error waiting for file to finish {}", newFile, e);
                                        }
                                    });

                                }

                            }

                        } finally {
                            key.reset();
                        }
                    }
                }

            } catch (Exception e) {
                LOGGER.error("Watcher thread died", e);
            } finally {
                mRunning = false;
            }
            
        }
    };

    private boolean isCompletelyWritten(File file) throws InterruptedException {
        long fileSizeBefore = file.length();
        Thread.sleep(3000);
        long fileSizeAfter = file.length();

        return fileSizeBefore == fileSizeAfter;
    }


}
