package com.videray.otaupdate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;


@Configuration
@EnableConfigurationProperties({
        Config.class
})
@SpringBootApplication
public class OTAApp {

    private static final Logger LOGGER = LoggerFactory.getLogger(OTAApp.class);
    public static final String UPDATEFILES_DIR = "updateFilesDir";
    public static final String TARGETFILES_DIR = "targetFilesDir";
    public static final String DROP_DIR = "dropDir";
    public static final String LOG_DIR = "logDir";


    public static void main(String[] args) {
        ConfigurableApplicationContext appContext = SpringApplication.run(OTAApp.class, args);

        //for(String beanName : appContext.getBeanDefinitionNames()) {
        //    System.out.println("bean: " + beanName);
        //}

    }

    @Bean(name = "root")
    public File rootDir() {
        File retval = new File("otaupdates");
        if(!retval.exists()) {
            retval.mkdirs();
        }
        return retval;
    }


    @Bean(name = UPDATEFILES_DIR)
    public File getUpdateFilesDir() {
        File retval = new File(rootDir(), "updateFiles");
        if(!retval.exists()) {
            retval.mkdirs();
        }
        return retval;
    }

    @Bean(name = TARGETFILES_DIR)
    public File getTargetFilesDir() {
        File retval = new File(rootDir(), "targetFiles");
        if(!retval.exists()) {
            retval.mkdirs();
        }
        return retval;
    }

    @Bean(name = DROP_DIR)
    public File getDropFilesDir() {
        File retval = new File(rootDir(), "drop");
        if(!retval.exists()) {
            retval.mkdirs();
        }
        return retval;
    }

    @Bean(name = LOG_DIR)
    public File getLogFilesDir() {
        File retval = new File(rootDir(), "logs");
        if(!retval.exists()) {
            retval.mkdirs();
        }
        return retval;
    }

    @Bean
    public File indexFile() {
        return new File(rootDir(), "index.json");
    }



}
