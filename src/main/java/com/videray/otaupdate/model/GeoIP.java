package com.videray.otaupdate.model;

import lombok.Data;

@Data
public class GeoIP {

    private String ipAddress;
    private String device;
    private String city;
    public String fullLocation = null;;
    private Double latitude;
    private Double longitude;
    public void setCity(String name) {
        city=name;
    }
    public void setFullLocation(String location) {
        fullLocation = location;
    }
    public void setLatitude(double d) {
        latitude=d;
    }
    public void setLongitude(double d) {
        longitude=d;
    }
    public void setIpAddress(String ip) {
        ipAddress=ip;
    }

}