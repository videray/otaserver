package com.videray.otaupdate.api;

public class UpdateResponse {
    public boolean available = false;
    public String channel;
    public String version;
    public String release_notes;
    public String downloadId;
}
