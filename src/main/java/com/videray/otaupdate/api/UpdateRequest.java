package com.videray.otaupdate.api;

public class UpdateRequest {
    public String device;
    public String channel;
    public String current_version;
    public String current_channel;
}
