package com.videray.otaupdate;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties
public class Config {

    private String signingKey;
    private String pathPrefix;
    private boolean removeDeflate = true;

    public String getSigningKey() {
        return signingKey;
    }

    public void setSigningKey(String signingKey) {
        this.signingKey = signingKey;
    }

    public String getPathPrefix() {
        return pathPrefix;
    }

    public void setPathPrefix(String pathPrefix) {
        this.pathPrefix = pathPrefix;
    }

    public boolean isRemoveDeflate() {
        return removeDeflate;
    }

    public void setRemoveDeflate(boolean removeDeflate) {
        this.removeDeflate = removeDeflate;
    }
}
