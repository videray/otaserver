package com.videray.otaupdate.service;

import java.io.IOException;
import java.net.UnknownHostException;

import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.videray.otaupdate.model.GeoIP;

public interface GeoIPLocationService {
    GeoIP getIpLocation(String ip) throws GeoIp2Exception, UnknownHostException, IOException;
}
