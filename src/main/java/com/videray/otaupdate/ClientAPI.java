package com.videray.otaupdate;

import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.videray.otaupdate.api.UpdateRequest;
import com.videray.otaupdate.api.UpdateResponse;
import com.videray.otaupdate.service.GeoIPLocationService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import org.springframework.core.env.Environment;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

@RestController
@RequestMapping("api/v1")
public class ClientAPI {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientAPI.class);


    @Autowired
    private UpdateManager updateManager;

    @Autowired
    private Environment env;
    @Autowired
    @Qualifier(OTAApp.LOG_DIR)
    private File logDir;
    private GeoIPLocationService geoIPLocationService;

    public ClientAPI(GeoIPLocationService geoIPLocationService) {
        this.geoIPLocationService = geoIPLocationService;
    }

    @RequestMapping(value = "update", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public UpdateResponse index(HttpServletRequest request, HttpServletResponse response, @RequestBody UpdateRequest updateRequest) throws IOException {
        LOGGER.info("client {} ask for update {} {} {}", request.getRemoteHost(),
                updateRequest.device,
                updateRequest.channel,
                updateRequest.current_version
                );

        // BufferedWriter bw = null;
        // try 
        // {
        //     bw = new BufferedWriter(new FileWriter(logDir+"/downloads.dat", true));
        //     bw.write("Serving Client: " + "\r\n");
        //     bw.newLine();
        //     bw.flush();
        // } catch ( IOException e )
        // {
        
        // } finally {                       // always close the file
        //     if (bw != null) try {
        //          bw.close();
        //         } catch (IOException ioe2) {
        //         // just ignore it
        //         }       
        //  } // end try/catch/finally
         
        UpdateResponse retval = new UpdateResponse();

        VersionKey current_version = new VersionKey(updateRequest.device, updateRequest.channel, Version.fromString(updateRequest.current_version));

        Version key = updateManager.getLatestFor(updateRequest.device, updateRequest.channel);
        if(key != null && key.compareTo(current_version.version) > 0) {
            //newer version exists
            retval.available = true;


            final VersionKey new_version = new VersionKey(updateRequest.device, updateRequest.channel, key);

            retval.version = new_version.version.toString();
            retval.channel = new_version.channel;
            retval.release_notes = "";

            if(updateManager.hasTargetFiles(current_version) && updateManager.hasTargetFiles(new_version)) {
                retval.downloadId = updateManager.getIncrementalUpdate(current_version, new_version);
            } else {
                retval.downloadId = updateManager.getFullUpdate(new_version);
            }


        } else {
            retval.available = false;
            response.setStatus(201);
        }

        return retval;
    }

    @RequestMapping(value = "download/{id}", method = RequestMethod.GET)
    public void download(HttpServletRequest request, HttpServletResponse response, @PathVariable("id") String id) throws IOException {

        UpdateFile updateFile = updateManager.getUpdateFile(id);
        if(updateFile == null) {
            LOGGER.warn("update file does not exist {} request from {}", id, request.getRemoteHost());
            response.sendError(404);
            return;
        }

        LOGGER.info("client {} ask to download {}", request.getRemoteHost(), updateFile);

        switch (updateFile.getStatus()){
            case Error:
                LOGGER.error("update file in error status: {}", updateFile);
                response.sendError(500);
                return;

            case Processing:
                LOGGER.info("update file is processing: {}", updateFile);
                response.addHeader("Retry-After", "120");
                response.sendError(302);
                return;

            case Ready:
                FileSender.fromFile(updateFile.getLocalFile(), geoIPLocationService, logDir)
                        .with(request)
                        .with(response)
                        .serve();
                return;

        }

        // @RequestMapping(value = "/getdownloads", method = RequestMethod.GET)
        // public String listContact(Model model) {
        //     model.addAttribute("contacts", "Bijal");   
        //     return "downloads";
        // }
    }

    @RequestMapping(value = "viewdownloads", method = RequestMethod.GET)
    public void viewDownloads(HttpServletRequest request, HttpServletResponse response) throws IOException {
       response.addHeader("content-type", "text/plain; charset=utf-8");
        response.setStatus(200);

        BufferedReader reader = null;
        PrintWriter out = response.getWriter();

        try {
            reader = new BufferedReader( new FileReader(logDir+"/downloads.dat"));
            String line = reader.readLine();
            while (line != null) {
                out.println(line);
                out.write("\r\n");
                out.println();
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e){
        }
        out.close();
        return;
    }

}
