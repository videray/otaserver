package com.videray.otaupdate;

import com.google.common.collect.ComparisonChain;

public class VersionKey implements Comparable<VersionKey> {

    public final String device;
    public final String channel;
    public final Version version;

    public VersionKey(String device, String channel, Version version) {
        this.device = device;
        this.channel = channel;
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if(o.getClass() != getClass()) {
            return false;
        } else {
            return compareTo((VersionKey) o) == 0;
        }
    }

    @Override
    public int hashCode() {
        return device.hashCode() ^ channel.hashCode() ^ version.hashCode();
    }

    @Override
    public String toString() {
        return String.format("%s_%s_%s", device, channel, version);
    }

    @Override
    public int compareTo(VersionKey o) {
        return ComparisonChain.start()
                .compare(device, o.device)
                .compare(channel, o.channel)
                .compare(version, o.version)
                .result();
    }
}
