package com.videray.otaupdate;

import com.google.common.base.MoreObjects;
import com.google.common.hash.HashCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.concurrent.Future;

class UpdateFile {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateFile.class);


    public enum Status {
        Processing,
        Ready,
        Error
    }

    private Status mStatus;
    private File localFile;
    private HashCode hashCode;


    public UpdateFile() {
        mStatus = Status.Processing;
    }

    public synchronized Status waitForProcessingComplete() {
        while(mStatus == Status.Processing) {
            try {
                wait(500);
            } catch (InterruptedException e) {
                LOGGER.error("", e);
            }
        }
        return mStatus;
    }

    public synchronized Status getStatus() {
        return mStatus;
    }

    public synchronized void setStatus(Status status) {
        mStatus = status;
        notifyAll();
    }

    public File getLocalFile() {
        return localFile;
    }

    public void setLocalFile(File localFile) {
        this.localFile = localFile;
    }

    public HashCode getHashCode() {
        return hashCode;
    }

    public void setHashCode(HashCode hashCode) {
        this.hashCode = hashCode;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("status", mStatus)
                .add("localFile", localFile)
                .add("hash", hashCode)
                .toString();
    }
}
