package com.videray.otaupdate;

import com.google.common.base.Preconditions;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import com.videray.otaupdate.config.GeoLocationConfig;
import com.videray.otaupdate.model.GeoIP;
import com.videray.otaupdate.service.GeoIPLocationService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Locale;

public class FileSender {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileSender.class);
    private  GeoIPLocationService geoIPLocationService;
    
    
    private File mLocalFile;
    private File mLogDir;
    private HttpServletRequest mRequest;
    private HttpServletResponse mResponse;

    public static FileSender fromFile(File file, GeoIPLocationService locationService, File logDir) {
        FileSender retval = new FileSender();
        retval.geoIPLocationService = locationService;
        retval.mLocalFile = file;
        retval.mLogDir = logDir;
        return retval;
    }

    public FileSender with(HttpServletRequest request) {
        mRequest = request;
        return this;
    }

    public FileSender with(HttpServletResponse response) {
        mResponse = response;
        return this;
    }

    public void serve() throws IOException {


        mResponse.setHeader("Content-Length", Long.toString(mLocalFile.length()));

        FileInputStream in = new FileInputStream(mLocalFile);
        ServletOutputStream out = mResponse.getOutputStream();

        byte[] buffer = new byte[4096];
        int len;
        while((len = in.read(buffer, 0, buffer.length)) > 0) {
            out.write(buffer, 0, len);
        }

        in.close();
        out.flush();

        LOGGER.warn("serving Bijal {} to {}", mLocalFile, mRequest.getRemoteAddr());

        GeoIP geoIP = null;
        BufferedWriter bw = null;
        String curr_date;
        DateFormat DFormat = DateFormat.getDateTimeInstance(
                DateFormat.LONG, DateFormat.LONG,
                Locale.getDefault());
        // Initializing the calender Object
        Calendar cal = Calendar.getInstance();  
        curr_date = DFormat.format(cal.getTime());
       
        try 
        {
            bw = new BufferedWriter(new FileWriter(mLogDir+"/downloads.dat", true));
            geoIP = geoIPLocationService.getIpLocation(mRequest.getRemoteAddr());           
            bw.write("Serving Client: " + curr_date + " " + geoIP.fullLocation + " " + mLocalFile);
            LOGGER.info("Writing to downloads.dat");
            bw.newLine();
            bw.flush();
        } catch ( GeoIp2Exception e )
        {
            bw.write("Serving Client: " + curr_date + " " + mLocalFile);
            bw.newLine();
            bw.flush();
        } finally {                       // always close the file
            if (bw != null) try {
                 bw.close();
                } catch (IOException ioe2) {
                // just ignore it
                }       
         } // end try/catch/finally


    }






}
