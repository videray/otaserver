package com.videray.otaupdate;


import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.io.Files;
import com.videray.otaupdate.api.AddTargetFileRequest;
import com.videray.otaupdate.tasks.RemoveDeflateTask;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;

@RestController
@RequestMapping("admin/v1")
public class AdminController {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(AdminController.class);

    @Autowired
    private UpdateManager updateManager;

    @RequestMapping(value = "add", method = RequestMethod.POST, consumes = "application/json")
    public void add(HttpServletRequest request, HttpServletResponse response, @RequestBody AddTargetFileRequest addRequest) {

        LOGGER.info("add: {}", MoreObjects.toStringHelper(addRequest)
                .add("path", addRequest.path)
                .add("removeDeflate", addRequest.removeDeflate)
                .toString());

        File targetFile = new File(addRequest.path);
        if(!targetFile.exists()) {
            LOGGER.error("file does not exist: {}", targetFile.getAbsolutePath());
            response.setStatus(400);
            return;
        }

        if(addRequest.removeDeflate) {

            Static.workers.execute( () -> {

                try {
                    VersionKey versionKey = UpdateManager.createVersionKey(targetFile);
                    RemoveDeflateTask removeDeflateTask = new RemoveDeflateTask();
                    removeDeflateTask.inputZipFile = targetFile;
                    removeDeflateTask.outputZipFile = updateManager.getTargetFilesZip(versionKey);
                    removeDeflateTask.run();

                    updateManager.addVersion(versionKey);

                } catch (Exception e) {
                    LOGGER.error("", e);
                }

            });

        } else {
            Static.workers.execute(() ->{

                try {
                    VersionKey versionKey = UpdateManager.createVersionKey(targetFile);
                    File dest_file = updateManager.getTargetFilesZip(versionKey);
                    Files.move(targetFile, dest_file);
                    updateManager.addVersion(versionKey);
                } catch (Exception e) {
                    LOGGER.error("", e);
                }

            });
        }




    }

}
