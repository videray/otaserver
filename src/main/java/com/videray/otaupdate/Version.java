package com.videray.otaupdate;

import com.google.common.base.Preconditions;
import com.google.common.collect.ComparisonChain;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Version implements Comparable<Version> {

    private static final Pattern REGEX = Pattern.compile("(\\d+)\\.(\\d+)\\.(\\d+)");

    public static Version fromString(String str) throws IOException {
        Version retval = null;

        Matcher m = REGEX.matcher(str);
        if(m.find()) {
            int major = Integer.valueOf(m.group(1));
            int minor = Integer.valueOf(m.group(2));
            int patch = Integer.valueOf(m.group(3));
            retval = new Version(major, minor, patch);
        }

        return retval;
    }

    public static final Version MAX = new Version(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE);

    public final int major;
    public final int minor;
    public final int patch;

    public Version(int major, int minor, int patch) {
        Preconditions.checkArgument(major >= 0);
        Preconditions.checkArgument(minor >= 0);
        Preconditions.checkArgument(patch >= 0);
        this.major = major;
        this.minor = minor;
        this.patch = patch;
    }

    @Override
    public boolean equals(Object o) {
        if(o.getClass() != getClass()) {
            return false;
        } else {
            return compareTo((Version) o) == 0;
        }
    }

    @Override
    public int hashCode() {
        int retval = major ^ minor ^ patch;
        return retval;
    }

    @Override
    public String toString() {
        return String.format("%d.%d.%d", major, minor, patch);
    }

    @Override
    public int compareTo(Version o) {
        return ComparisonChain.start()
                .compare(major, o.major)
                .compare(minor, o.minor)
                .compare(patch, o.patch)
                .result();
    }
}
