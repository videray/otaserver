package com.videray.otaupdate;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Service
public class IndexService {

    private static final Logger LOGGER = LoggerFactory.getLogger(IndexService.class);
    private final ScheduledExecutorService mExecutorService = Executors.newScheduledThreadPool(3);

    @Autowired
    UpdateManager updateManager;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    File targetFilesDir;

    @Autowired
    File indexFile;


    private static class UpdateFile {
        public String path;
        public String device;
        public String channel;
        public String version;
    }

    private static class IndexFile {
        UpdateFile[] updateFiles;
    }

    public void readIndexFile(File indexFile) {
        try {
            IndexFile index = objectMapper.readValue(indexFile, IndexFile.class);

            for (UpdateFile targetFiles : index.updateFiles) {
                VersionKey key = new VersionKey(targetFiles.device, targetFiles.channel, Version.fromString(targetFiles.version));
                File updateFile = new File(targetFiles.path);
                if (updateFile.exists()) {
                    updateManager.addFullUpdateFile(key, updateFile);
                } else {
                    LOGGER.error("file does not exist: {}", updateFile.getAbsolutePath());
                }

            }
        } catch (IOException e) {
            LOGGER.error("", e);
        }
    }

    @PostConstruct
    public void init() {
        scanTargetFilesDir(targetFilesDir);
        if(indexFile != null && indexFile.exists()) {
            updateManager.getExecutor().execute(() -> readIndexFile(indexFile));
        }
    }

    public void scanTargetFilesDir(File targetFilesDir) {
        File[] files = targetFilesDir.listFiles();
        if(files != null) {
            for(File f : files) {
                updateManager.getExecutor().execute(() -> processTargetFilesFile(f));
            }
        }
    }

    private void processTargetFilesFile(File f) {
        try {
            VersionKey versionKey = UpdateManager.createVersionKey(f);
            updateManager.addVersion(versionKey);
        } catch (Exception e) {
            LOGGER.error("", e);
        }
    }

}
