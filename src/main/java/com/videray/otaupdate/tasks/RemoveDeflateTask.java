package com.videray.otaupdate.tasks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class RemoveDeflateTask implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(RemoveDeflateTask.class);

    public File inputZipFile;
    public File outputZipFile;
    public boolean success;

    @Override
    public void run() {

        success = false;
        try {

            byte[] buf = new byte[2048];
            ZipEntry ze;
            int len;

            ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(outputZipFile));
            zipOut.setMethod(ZipOutputStream.STORED);


            FileInputStream fin = new FileInputStream(inputZipFile);
            ZipInputStream zipIn = new ZipInputStream(new BufferedInputStream(fin));


            File tmpFile = File.createTempFile("unziptmpfile", ".dat");
            try {

                while ((ze = zipIn.getNextEntry()) != null) {

                    try (FileOutputStream tmpOut = new FileOutputStream(tmpFile)) {
                        while ((len = zipIn.read(buf)) > 0) {
                            tmpOut.write(buf, 0, len);
                        }
                    }

                    ze.setMethod(ZipOutputStream.STORED);
                    ze.setCompressedSize(ze.getSize());
                    zipOut.putNextEntry(ze);

                    try (FileInputStream tmpIn = new FileInputStream(tmpFile)) {
                        while ((len = tmpIn.read(buf)) > 0) {
                            zipOut.write(buf, 0, len);
                        }
                    }

                    zipOut.closeEntry();
                }
            } finally {
                tmpFile.delete();
                zipIn.close();
                zipOut.close();
            }

            success = true;

        } catch (Exception e) {
            success = false;
            LOGGER.error("", e);
        }

    }
}
