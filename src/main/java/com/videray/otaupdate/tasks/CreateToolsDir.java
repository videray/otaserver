package com.videray.otaupdate.tasks;

import com.google.common.io.Files;
import com.google.common.io.Resources;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class CreateToolsDir implements Runnable {

    public static final Logger LOGGER = LoggerFactory.getLogger(CreateToolsDir.class);

    private File mToolsDir;
    public boolean success;
    public File releaseToolsDir;

    @Override
    public void run() {
        success = false;

        try {
            mToolsDir = Files.createTempDir();

            releaseToolsDir = new File(mToolsDir, "releasetools");
            releaseToolsDir.mkdirs();

            ZipInputStream zin = new ZipInputStream(Resources.asByteSource(Resources.getResource("releasetools.zip")).openStream());
            try {
                ZipEntry entry;
                while ((entry = zin.getNextEntry()) != null) {
                    File entryDestination = new File(releaseToolsDir, entry.getName());
                    if (entry.isDirectory()) {
                        entryDestination.mkdirs();
                    } else {
                        entryDestination.getParentFile().mkdirs();
                        OutputStream out = new FileOutputStream(entryDestination);
                        IOUtils.copy(zin, out);
                        zin.closeEntry();

                        out.close();
                    }
                }
            } finally {
                zin.close();
            }

            success = true;

        } catch (Exception e) {
            LOGGER.error("", e);
        }

    }
}
