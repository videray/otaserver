package com.videray.otaupdate.tasks;

import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.io.Files;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Callable;

public class ComputeHash implements Callable<HashCode> {

    private final File file;
    private final HashFunction function;

    public ComputeHash(File f, HashFunction function) {
        this.file = f;
        this.function = function;
    }

    @Override
    public HashCode call() throws IOException {
        return Files.asByteSource(file).hash(function);
    }
}
