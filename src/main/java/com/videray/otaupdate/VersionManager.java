package com.videray.otaupdate;

import java.util.TreeSet;

public class VersionManager {

    private TreeSet<VersionKey> mVersions = new TreeSet<>();

    public void add(VersionKey key) {
        synchronized (mVersions) {
            mVersions.add(key);
        }
    }

    public Version getLatestFor(String device, String channel) {
        VersionKey key = new VersionKey(device, channel, Version.MAX);
        VersionKey latestVersion;
        synchronized (mVersions) {
            latestVersion = mVersions.floor(key);
        }

        if(latestVersion != null &&
                latestVersion.device.equals(device) &&
                latestVersion.channel.equals(channel)) {

            return latestVersion.version;

        } else {
            return null;
        }
    }
}
