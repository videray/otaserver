package com.videray.otaupdate;

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import com.videray.otaupdate.tasks.ComputeHash;
import com.videray.otaupdate.tasks.CreateToolsDir;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.TreeMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;


@Service
public class UpdateManager {

    public static final Logger LOGGER = LoggerFactory.getLogger(UpdateManager.class);
    public static final String BUILD_PROP_NAME = "SYSTEM/build.prop";
    public static final HashFunction NAME_HASH_FUNCTION = Hashing.murmur3_32();
    public static final HashFunction FILE_HASH_FUNCTION = Hashing.sha1();

    public static VersionKey createVersionKey(File targetfilesZip) throws IOException {
        Preconditions.checkNotNull(targetfilesZip);
        LOGGER.info("loading target files zip {}", targetfilesZip.getAbsolutePath());

        Properties properties = loadBuildProp(targetfilesZip);

        String device = properties.getProperty("ro.product.device");
        LOGGER.info("device: {}", device);

        String channel = properties.getProperty("ro.build.channel");
        LOGGER.info("channel: {}", channel);

        String versionStr = properties.getProperty("ro.build.version.id");
        LOGGER.info("version: {}", versionStr);

        Preconditions.checkNotNull(device);
        Preconditions.checkNotNull(channel);
        Preconditions.checkNotNull(versionStr);
        Version version = Version.fromString(versionStr);
        Preconditions.checkNotNull(version);


        VersionKey key = new VersionKey(device, channel, version);
        return key;
    }

    public static Properties loadBuildProp(File targetFilesZip) throws IOException {
        Properties retval = new Properties();

        try(ZipFile zipFile = new ZipFile(targetFilesZip)) {
            ZipEntry entry = zipFile.getEntry(BUILD_PROP_NAME);
            if(entry == null) {
                throw new IOException(BUILD_PROP_NAME + " not found");
            }

            retval.load(zipFile.getInputStream(entry));
        }

        return retval;

    }

    private final ScheduledExecutorService mExecutor = Executors.newSingleThreadScheduledExecutor();
    private CreateToolsDir mTools;

    @Autowired
    @Qualifier(OTAApp.UPDATEFILES_DIR)
    File mUpdateFilesDir;

    @Autowired
    @Qualifier(OTAApp.TARGETFILES_DIR)
    File mTargetFilesDir;

    @Autowired
    Config config;

    private VersionManager mVersionManager = new VersionManager();
    private TreeMap<String, UpdateFile> mUpdateFiles = new TreeMap<>();

    public UpdateManager() {
        mTools = new CreateToolsDir();
        mExecutor.execute(mTools);
    }

    public ScheduledExecutorService getExecutor() {
        return mExecutor;
    }

    public void addVersion(VersionKey key) {
        mVersionManager.add(key);
    }

    public Version getLatestFor(String device, String channel) {
        return mVersionManager.getLatestFor(device, channel);
    }

    public UpdateFile getUpdateFile(String id) {
        return mUpdateFiles.get(id);
    }


    private static String sanitizeFilename(String input) {
        return input.replaceAll("[^a-zA-Z0-9]", "_");
    }


    public File getTargetFilesZip(VersionKey key) {
        String filename = String.format("targetfiles_%s.zip", sanitizeFilename(key.toString()));
        return new File(mTargetFilesDir, filename);
    }

    private File getFullUpdateFile(VersionKey key) {
        String filename = String.format("full_%s.zip", sanitizeFilename(key.toString()));
        return new File(mUpdateFilesDir, filename);
    }

    private File getIncrementalUpdateFile(VersionKey from, VersionKey to) {
        String filename = String.format("inc_%s-%s.zip", sanitizeFilename(from.toString()), sanitizeFilename(to.toString()));
        return new File(mUpdateFilesDir, filename);
    }

    public void addFullUpdateFile(VersionKey key, File f) {
        final String updateKey = NAME_HASH_FUNCTION.hashString(key.toString(), Charsets.UTF_8).toString();
        UpdateFile updateFile = new UpdateFile();
        updateFile.setLocalFile(f);

        mExecutor.execute(() -> {
            try {
                updateFile.setHashCode(new ComputeHash(f, FILE_HASH_FUNCTION).call());
                updateFile.setStatus(UpdateFile.Status.Ready);
            } catch (Exception e) {
                updateFile.setStatus(UpdateFile.Status.Error);
            }

        });

        synchronized (this) {
            mUpdateFiles.put(updateKey, updateFile);
        }
    }

    public String getFullUpdate(VersionKey key) {
        final String updateKey = NAME_HASH_FUNCTION.hashString(key.toString(), Charsets.UTF_8).toString();

        synchronized (this) {
            if(!mUpdateFiles.containsKey(updateKey)) {
                CreateFullUpdate createTask = new CreateFullUpdate(key);
                createTask.targetFilesZip = getTargetFilesZip(key);
                createTask.finalUpdateFile = getFullUpdateFile(key);
                createTask.updateFileObj = new UpdateFile();
                mUpdateFiles.put(updateKey, createTask.updateFileObj);
                mExecutor.execute(createTask);
            }
        }

        return updateKey;
    }

    public String getIncrementalUpdate(VersionKey from, VersionKey to) {
        final String updateFileKey = NAME_HASH_FUNCTION.hashString(String.format("%s==>%s", from, to), Charsets.UTF_8).toString();

        synchronized (this) {
            if(!mUpdateFiles.containsKey(updateFileKey)) {
                CreateIncrementalUpdate createTask = new CreateIncrementalUpdate(from, to);
                createTask.fromTargetFiles = getTargetFilesZip(from);
                createTask.toTargetFiles = getTargetFilesZip(to);
                createTask.finalUpdateFile = getIncrementalUpdateFile(from, to);
                createTask.updateFileObj = new UpdateFile();
                mUpdateFiles.put(updateFileKey, createTask.updateFileObj);
                mExecutor.execute(createTask);
            }
        }

        return updateFileKey;
    }

    public boolean hasTargetFiles(VersionKey key) {
        File targetFiles = getTargetFilesZip(key);
        return targetFiles.exists();
    }

    private abstract class CreateUpdateFile implements Runnable {
        public File finalUpdateFile;
        public UpdateFile updateFileObj;

        abstract List<String> createCommandLine();

        @Override
        public void run() {
            try {
                LOGGER.info("begin creating update file: {}", finalUpdateFile.getName());
                Process process = new ProcessBuilder()
                        .directory(mTools.releaseToolsDir)
                        .command(createCommandLine())
                        .inheritIO()
                        .start();

                int exitValue = process.waitFor();
                LOGGER.info("process exited with: {}", exitValue);
                if (exitValue != 0) {
                    throw new IOException(String.format("process return non-zero"));
                }

                updateFileObj.setHashCode(new ComputeHash(finalUpdateFile, FILE_HASH_FUNCTION).call());
                updateFileObj.setLocalFile(finalUpdateFile);
                updateFileObj.setStatus(UpdateFile.Status.Ready);

                LOGGER.info("update file ready: {}", finalUpdateFile.getName());

            } catch (Exception e) {
                LOGGER.error("error creating update file", e);
                updateFileObj.setStatus(UpdateFile.Status.Error);
            }
        }

    }

    private class CreateFullUpdate extends CreateUpdateFile {

        final VersionKey key;
        File targetFilesZip;

        public CreateFullUpdate(VersionKey key) {
            this.key = key;
        }

        @Override
        List<String> createCommandLine() {
            ArrayList<String> commandline = new ArrayList<String>();
            commandline.add("python");
            commandline.add("ota_from_target_files.py");

            String keypath = config.getSigningKey();
            if(keypath != null) {
                commandline.add("-k");
                commandline.add(keypath);
            }

            String pathPrefix = config.getPathPrefix();
            if(pathPrefix != null) {
                commandline.add("-p");
                commandline.add(pathPrefix);
            }

            if(LOGGER.isDebugEnabled()) {
                commandline.add("-v");
            }

            commandline.add(targetFilesZip.getAbsolutePath());
            commandline.add(finalUpdateFile.getAbsolutePath());
            return commandline;
        }

    }

    private class CreateIncrementalUpdate extends CreateUpdateFile {

        public final VersionKey from;
        public final VersionKey to;
        File fromTargetFiles;
        File toTargetFiles;

        public CreateIncrementalUpdate(VersionKey from, VersionKey to) {
            this.from = from;
            this.to = to;
        }

        @Override
        List<String> createCommandLine() {
            ArrayList<String> commandline = new ArrayList<String>();
            commandline.add("python");
            commandline.add("ota_from_target_files.py");

            String keypath = config.getSigningKey();
            if(keypath != null) {
                commandline.add("-k");
                commandline.add(keypath);
            }

            String pathPrefix = config.getPathPrefix();
            if(pathPrefix != null) {
                commandline.add("-p");
                commandline.add(pathPrefix);
            }

            if(LOGGER.isDebugEnabled()) {
                commandline.add("-v");
            }

            commandline.add("-i");
            commandline.add(fromTargetFiles.getAbsolutePath());
            commandline.add(toTargetFiles.getAbsolutePath());
            commandline.add(finalUpdateFile.getAbsolutePath());
            return commandline;
        }
    }


}
